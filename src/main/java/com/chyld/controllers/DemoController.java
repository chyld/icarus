package com.chyld.controllers;

import com.chyld.entities.Exercise;
import com.chyld.entities.Profile;
import com.chyld.entities.User;
import com.chyld.security.JwtToken;
import com.chyld.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Collections;
import java.util.Map;

@RestController
public class DemoController {
    @Autowired
    private UserService service;

    @RequestMapping(value = "/getuser", method = RequestMethod.GET)
    public User demo_a(Principal user){
        int uid = ((JwtToken)user).getUserId();
        User u = service.findUserById(uid);
        return u;
    }

    @RequestMapping(value = "/createprofile", method = RequestMethod.POST)
    public User demo_b(Principal user, @RequestBody Profile profile){
        int uid = ((JwtToken)user).getUserId();
        User u = service.findUserById(uid);
        u.setProfile(profile);
        profile.setUser(u);
        return service.saveUser(u);
    }

    @RequestMapping(value = "/createexercise", method = RequestMethod.POST)
    public User demo_c(Principal user, @RequestBody Exercise exercise){
        int uid = ((JwtToken)user).getUserId();
        User u = service.findUserById(uid);
        exercise.setUser(u);
        u.getExercises().add(exercise);
        return service.saveUser(u);
    }

    @RequestMapping(value = "/demo2", method = RequestMethod.GET)
    public int demo2(){
        return 2;
    }

    @RequestMapping(value = "/demo3", method = RequestMethod.GET)
    public int demo3(){
        return 3;
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

    @RequestMapping("/token")
    @ResponseBody
    public Map<String,String> token(HttpSession session) {
        return Collections.singletonMap("token", session.getId());
    }
}
